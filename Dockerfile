FROM python:3.10-alpine

WORKDIR /code

COPY requirements.txt /code/requirements.txt
COPY ./zotero-rendering zotero_rendering

RUN pip install --no-cache-dir --upgrade -r /code/requirements.txt
RUN pip install -r /code/zotero_rendering/requirements.txt

COPY ./src /code
COPY ./assets assets
COPY ./private-assets/xml assets/xml/
COPY ./private-assets/html assets/html
COPY ./private-assets/img assets/img

RUN mkdir -p /code/assets/json
RUN python3 load_zotero_data.py

CMD ["uvicorn", "textapi:app", "--host", "0.0.0.0", "--port", "8084"]