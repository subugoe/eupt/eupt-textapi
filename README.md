# eupt-textapi

eupt-textapi is a FastAPI application that delivers the [TextAPI](https://textapi.sub.uni-goettingen.de) for the [EUPT project](https://uni-goettingen.de/de/eupt/672442.html).

The development instance of eupt-textapi is available at <https://eupt-dev.sub.uni-goettingen.de/api/eupt/collection.json>.

## Local Installation

0. Load the submodule with `git submodule update --init`.
1. Build the image with `docker build -t eupt-textapi .`.
2. Run the image with `docker run --rm -p 8084:8084 eupt-textapi`. The application is now available at `localhost:8084/api/eupt/collection.json`.

## Releases

In order to release a new version of the TextAPI to the production server, perform the following steps:

1. Merge the `develop` branch into `main`.
2. Take note of the new version created automatically. This version number is stated in the commit message that is created by the GitLab CI Template.
3. Switch to the `eupt-textapi-helm` repository and pull the latest changes of `main`.
4. Create and push a tag pointing to the most recent commit with the following structure: `textapi-X.Y.Z` where `X.Y.Z.` is the version number created in step 2.
5. Switch to the `argocd-provisioning` repository. Pull the latest changes of `main` and create a new branch for the version update.
6. In `deployments/prod/applications/templates/eupt-textapi.yaml` change the `targetRevision` value to the tag name you created in step 4. Save, commit, push.
7. Create a merge request in `argocd-provisioning` pointing to `main`.
8. Merge. @mgoebel said it's okay to simply merge branches where only a version update takes place.

ArgoCD will take a while before the changes apply.

:warning: For a full update of the EUPT website, a release must be made both for
the TextAPI and the website.

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[GNU APGLv3](LICENSE)
