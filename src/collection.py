"""
This module is responsible for creating the complete Collection Object
for the EUPT project.
"""

from constants import API_PREFIX, JSONLD_URL, TEXTAPI_VERSION
from util import get_all_manifest_ids, get_published_manifest_ids, get_display_id

def make_collection(eupt_url: str, eupt_server: str) -> dict:
    """
    Creates the Collection Object for the EUPT project.

    Args:
        - eupt_url: the base url
        - eupt_server: the server on which the application runs on (eupt, eupt-test, eupt-dev)
    """
    return {
        '@context': f'{JSONLD_URL}/collection.jsonld',
        'textapi': TEXTAPI_VERSION,
        'id': f'{eupt_url}{API_PREFIX}/collection.json',
        'title': [{
            '@context': f'{JSONLD_URL}/title.jsonld',
            'title': 'Edition des ugaritischen poetischen Textkorpus (EUPT)',
            'type': 'main'
        }],
        'collector': [{
            '@context': f'{JSONLD_URL}/actor.jsonld',
            'role': 'collector',
            'name': 'EUPT-Projektteam'
        }],
        'sequence': make_sequence(eupt_url, eupt_server)
    }


def make_sequence(eupt_url: str, eupt_server: str) -> list:
    """
    Creates the TextAPI Sequence Objects.

    Args:
        - eupt_url: the base url
        - eupt_server: the server on which the application runs on (eupt, eupt-staging, eupt-test, eupt-dev)
    """
    sequence = []
    for manifest in get_manifests(eupt_server):
        seq_obj = {
            '@context': f'{JSONLD_URL}/sequence.jsonld',
            'id': f'{eupt_url}{API_PREFIX}/{manifest}/manifest.json',
            'type': 'manifest',
            'label': get_display_id(manifest)
        }
        sequence.append(seq_obj)
    return sequence


def get_manifests(eupt_server: str) -> list:
    """
    Provides all manifests to be considered for the Sequence Object.
    Only manifests with a `docStatus/@status = 'published'` are considered and shown on 
    the production and staging instances.

    Args:
        - eupt_server: the server on which the application runs on ("eupt" (prod), 
            "eupt-staging", "eupt-test", "eupt-dev")
    """
    if eupt_server in ('eupt', 'eupt-staging', 'eupt-dev'):
        manifests =  get_published_manifest_ids()
    else:
        manifests = get_all_manifest_ids()
    return _sort_manifests(manifests)

def _sort_manifests(manifests: list) -> list:
    """
    Manifests have an abbreviation (KTU, RS) and a number (1.14, 25.460).
    They should be sorted ascending according to the following criteria:
        1. abbreviation
        2. first part of the number (before the dot)
        3. second part of the number (after the dot)
    Args:
        - manifests: a list of manifest IDs to be displayed
    """
    ktus = [x for x in manifests if 'KTU' in x]
    rss = [x for x in manifests if 'RS' in x]
    bubble_sort(ktus)
    bubble_sort(rss)

    return ktus + rss


def bubble_sort(array: list) -> list:
    """
    A classic bubble sort.
    We compare both the first number of a catalogue entry (the book?) and the 
    second number (the clay tablet(s)).
    """
    n = len(array)

    for i in range(n):
        already_sorted = True
        
        for j in range(n - i - 1):
            split_current = array[j].replace('__', '.').split('.')
            split_next = array[j + 1].replace('__', '.').split('.')
            # the first part of the condition sorts the entries according to the books,
            # e.g. KTU 2.1 and KTU 1.4 are switched into the right order.
            # the second part of the condition takes care of the number after the dot,
            # e.g. KTU 1.12 and KTU 1.2 are switched into the right order.
            if int(split_current[1]) > int(split_next[1]) \
            or split_current[1] == split_next[1] and int(split_current[2].split('-')[0]) > int(split_next[2].split('-')[0]):
                array[j], array[j + 1] = array[j + 1], array[j]
                already_sorted = False
                
        if already_sorted:
            break
    return array