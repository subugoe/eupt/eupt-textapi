"""
This module is responsible for creating a complete Item Object
for the EUPT project.
"""

from constants import API_PREFIX, JSONLD_URL, TEXTAPI_VERSION
from util import get_normalized_id


def make_item(manifest_id: str, eupt_url: str) -> dict:
    """
    Creates an Item Object for the EUPT project.
    """
    norm_id = get_normalized_id(manifest_id)
    item = {
        '@context': f'{JSONLD_URL}/item.jsonld',
        'textapi': TEXTAPI_VERSION,
        'id': f'{eupt_url}{API_PREFIX}/{manifest_id}/1/full.json',
        'type': 'full',
        'lang': ['uga'],
        'content': make_content(norm_id, eupt_url)
    }
    # FEATURE FLAG
    # TODO: remove if image display is ready for production
    if 'eupt.uni-goettingen' in eupt_url:
        return item
    item['image'] = make_image(norm_id, eupt_url)
    return item

def make_image(norm_id: str, eupt_url: str) -> dict:
    """
    Creates the image object for the EUPT TextAPI.
    
    Args:
        - norm_id: the normalized ID, e.g. KTU_1.14
        - eupt_url: the current base URL, e.g. http://eupt.uni-goettingen.de
    """
    return {
        '@context': f'{JSONLD_URL}/image.jsonld',
        'id': f'{eupt_url}{API_PREFIX}/img/{norm_id}',
        'license': {
            'id': 'CC-BY-SA-4.0'
        }
    }

def make_content(norm_id: str, eupt_url: str) -> list:
    """
    Creates the TextAPI Content Objects.
    """
    sequence = []
    for mime_type in get_content_types():
        c_type = mime_type.rsplit('=', maxsplit=1)[-1]
        seq_obj = {
            '@context': f'{JSONLD_URL}/content.jsonld',
            'url': f'{eupt_url}{API_PREFIX}/html/{norm_id}_{c_type}.html',
            'type': mime_type
        }
        sequence.append(seq_obj)
    return sequence


def get_content_types():
    """
    Returns a list of all the content types used in EUPT.
    """
    return ['text/html;type=facsimile', 'text/html;type=philology']
