""""
Retrieves all necessary information for a given manuscript from
the respective main file of a manifest. This information encompasses:

- material properties
- text layout
- provenance of the tablet
- current location of the tablet (incl. repository number)
- information about the respective excavation
- publication numbers
- genre
- writer
- bibliographical references
- change history of the research data
"""

from datetime import datetime
from typing import Tuple
import logging
import re
import sys
from lxml import etree
import ujson as json
import zotero_rendering.src.biblio as z
from constants import NS, XML_NS
from util import get_normalized_id

sys.path.append('code/zotero_rendering')

logging.basicConfig(format="{levelname}: {pathname} {lineno} - {message}", style="{")

def get_metadata(manifest_id: str, eupt_url: str, xml: etree._Element) -> list:
    """Get the relevant metadata for an EUPT manifest.

    Args:
        manifest_id: ID of an EUPT manifest, e.g. "KTU__1.14".
        eupt_url: the base URL, e.g. https://eupt.uni-goettingen.de
        xml: the root element of the manifest's XML file

    Returns:
        List of objects containing all relevant information
    """
    norm_id = get_normalized_id(manifest_id)
    (obj_desc, prov, lang, meas, layout, writer, genre,
        excv, repo, pub) = get_general_manuscript_info(xml)
    biblio = get_bibliography(xml)
    surrogates = get_surrogates(xml)
    changes = get_history_of_changes(xml)
    cit = get_citation_hint(norm_id, eupt_url, xml)
    return [
        cit,
        obj_desc, prov, lang, meas, layout, writer, genre, excv, repo, pub,
        biblio,
        surrogates,
        changes
    ]


def get_general_manuscript_info(xml: etree._Element) -> tuple:
    """Get the general manuscript information from an XML file.

    Args:
        xml: the root element of the XML document
        norm_id: the normalized manuscript ID, e.g. "KTU_1.14"
    """
    obj_desc = get_object_desc(xml)
    prov = get_provenance(xml)
    layout = get_column_info(xml)
    measures = get_measures(xml)
    writer = get_writer(xml)
    genre = get_genre(xml)
    excv = get_excavation_no(xml)
    repo = get_repository(xml)
    repo_n = get_repository_no(xml)
    pub = get_publication_no(xml)
    return (
        {'key': obj_desc[0], 'value': obj_desc[1]},
        {'key': 'Fundort', 'value': prov},
        {'key': 'Schrift/Sprache', 'value': 'Keilalphabetisch / ugaritisch'},
        {'key': 'Maẞe', 'value': measures},
        {'key': layout[0], 'value': layout[1]},
        {'key': 'Schreiber', 'value': writer},
        {'key': 'Genre', 'value': genre},
        {'key': 'Grabungsnummer', 'value': excv},
        {'key': 'Museumssnummer', 'value': f'{repo_n} {repo}'},
        {'key': 'Publikationsnummer', 'value': pub}
    )


def get_elem_text(xml: etree._Element, element_name: str) -> str:
    """Retrieves the text of an XML element by element name.

    Args:
        xml: The root of the TEI-XML tree
        element_name: The local name of the element
    """
    return get_elem_text_xq(xml, f'.//{NS}{element_name}')


def get_elem_text_xq(xml: etree._Element, xpath: str) -> str:
    """Retrieves the text of an XML element by XPath.

    Args:
        xml: The root of the TEI-XML tree
        xpath: The XPath leading to an element
    """
    try:
        text = xml.find(xpath).text
        norm = normalize(text)
        return norm
    except AttributeError:
        logging.warning(f"No text found in {xpath}.")
        return '###'


def normalize(text: str) -> str:
    """
    Removes all unnecessary white spaces from a string.
    """
    try:
        pattern = re.compile(r'\s+')
        ws_collapsed = re.sub(pattern, ' ', text).strip(
        ).translate({ord(c): None for c in '\n\t\r'})
        normalized = re.sub(re.compile(r'\s\.'), '.', ws_collapsed)
        return normalized
    except TypeError:
        logging.warning(f'Text "{text}" could not be normalized. Text may be empty.')
        return '###'


def get_attrib_text(xml: etree._Element, element_name: str, attrib_name: str) -> str:
    """Retrieves the text of an XML attribute by element name.

    Args:
        xml: The root of the TEI-XML tree
        element_name
        attribute_name
    """
    attr_text = xml.find(f'.//{NS}{element_name}').attrib.get(attrib_name)
    if attr_text:
        return attr_text
    logging.warning(f"No attribute text found for {attrib_name} at {element_name}.")
    return '###'


def get_attrib_text_xq(xml: etree._Element, xpath: str, attrib_name: str):
    """Retrieves the text of an XML attribute by XPath.

    Args:
        xml: The root of the TEI-XML tree
        xpath: The XPath leading to the element that holds the attribute
        attribute_name
    """
    attr_text = xml.find(xpath).attrib.get(attrib_name)
    if attr_text:
        return attr_text
    logging.warning(f"No attribute text found for {attrib_name} at {xpath}.")
    return '###'


def get_object_desc(xml: etree._Element) -> Tuple[str, str]:
    """Get information about the physical dimensions and the
    of a tablet and its material from an XML file.

    Args:
        xml: the root element of the XML document
    """
    form = get_attrib_text(xml, 'objectDesc', 'form')
    material = get_elem_text(xml, 'support')
    if not form or not material:
        logging.warning("No form or material found.")
        form = '###'
        material = '###'
    obj_desc_key = f'{form} ({material})'
    return ('Schriftträger', obj_desc_key)


def get_measures(xml: etree._Element) -> str:
    """
    Get measurements for tablet.

    Args:
        xml: the root element of the XML document
    """
    height = get_elem_text(xml, 'height')
    width = get_elem_text(xml, 'width')
    depth = get_elem_text(xml, 'depth')
    if not (height and width and depth):
        logging.warning("No measurements found for tablet.")
    return f'{height} mm (H) x {width} mm (B) x {depth} mm (T)'


def get_column_info(xml: etree._Element) -> Tuple[str, str]:
    """Retrieves information about the layout of the tablet.

    Args:
        xml: The root of the TEI-XML tree
    """
    column_n = get_attrib_text(xml, 'layout', 'columns')
    column_info = get_elem_text(xml, 'layout')
    if not (column_n and column_info):
        logging.warning("No column number or information found.")
    return ('Textlayout', f'{column_n} Kolumnen; {column_info}')


def get_provenance(xml: etree._Element) -> str:
    """Retrieves information about the provenance of the tablet.

    Args:
        xml: The root of the TEI-XML tree
    """
    return string_from_mixed_content(xml.find(f'.//{NS}origin/{NS}p'))


def get_repository(xml: etree._Element) -> str:
    """Retrieves information about the current repository
     holding the tablet.

    Args:
        xml: The root of the TEI-XML tree
    """
    repo = get_elem_text(xml, 'repository')
    if not repo:
        logging.warning("No repository found.")
        repo = '###'
    return repo


def get_repository_no(xml: etree._Element) -> str:
    """Retrieves information about the current repository's number.

    Args:
        xml: The root of the TEI-XML tree
    """
    repo_no = get_elem_text(xml, 'idno[@type="Museumsnummer"]')
    if not repo_no:
        logging.warning("No repository number found.")
        repo_no = '###'
    return repo_no


def get_excavation_no(xml: etree._Element) -> str:
    """Retrieves information about the excavation number in with the
    tablet has been found.

    Args:
        xml: The root of the TEI-XML tree
    """
    exc = get_elem_text(xml, 'idno[@type="Grabungsnummer"]')
    if not exc:
        logging.warning("No excavation number found.")
        exc = '###'
    return exc


def get_publication_no(xml: etree._Element) -> str:
    """Retrieves information about the tablet's publication numbers.

    Args:
        xml: The root of the TEI-XML tree
    """
    no_els = list(xml.findall(f'.//{NS}idno[@type="Publikationsnummer"]'))
    if not no_els:
        logging.warning("No publication numbers found.")
    nos = [string_from_mixed_content(no) for no in no_els]
    return " = ".join(nos) + '<div class="padding"></div>' # insert padding before "Bibliographie"


def get_genre(xml: etree._Element) -> str:
    """Retrieves information about the genre of the text
    and an abstract.

    Args:
        xml: The root of the TEI-XML tree
    """
    if get_elem_text(xml, 'classCode') == 'epic':
        genre = 'Epos'
    else:
        genre = 'Sonstiges'
    abstr = xml.find(f'.//{NS}abstract/{NS}p')
    abstr_with_link = string_from_mixed_content(abstr)
    if not abstr:
        logging.warning("No abstract found.")
        abstr = '###'
    return f'{genre}. {abstr_with_link}'


def get_writer(xml: etree._Element) -> str:
    """Retrieves information about the text's writer.

    Args:
        xml: The root of the TEI-XML tree
    """
    wr_name = get_elem_text_xq(xml, f'.//{NS}handDesc/{NS}p')
    try:
        return wr_name.split(' ')[1]
    except IndexError:
        logging.warning("No writer found.")
        return "###"


def get_bibliography(xml: etree._Element) -> tuple:
    """Retrieves bibliographical references where users can find more
    information about the tablet.

    Args:
        xml: The root of the TEI-XML tree
    """
    with open('assets/json/eupt-bibliography.json', mode='r', encoding='utf-8') as f:
        zotero_data = json.loads(f.read())

    return ({'key': 'Bibliographie', 'metadata':
             [
                 {'key': 'Fotos', 'value': make_bibl(
                     xml, ['fotos'], zotero_data)},
                 {'key': 'Kopie', 'value': make_bibl(
                     xml, ['copies'], zotero_data)},
                 {'key': 'Studien', 'value': make_bibl(
                     xml, ['transliterations', 'translations', 'studies'], zotero_data)}
             ]
             }
            )


def make_bibl(xml: etree._Element, t: list, zot_data: list) -> str:
    """Retrieves all bibliograpical references of a certain type.

    Args:
        xml: The root of the TEI-XML tree
        t: The types of bibliographical reference, e.g. ['copies']
        zot_data: The complete Zotero bibliography as a list of dicts

    Return:
        A string that contains the references encoded in HTML
        unordered lists. These are rendered by TIDO later.
    """
    bibls = []
    for elem in t:
        bibls.append(xml.findall(
            f".//{NS}listBibl[@type='{elem}']/{NS}bibl[@zotero]"))
    if not bibls:
        logging.warning("No bibliographic entries found.")
    flattened_bibls = [x for nested_list in bibls for x in nested_list]
    list_of_refs = []
    for i, bibl in enumerate(flattened_bibls):
        norm = normalize(bibl.text)
        try:
            zot_id = bibl.attrib['zotero'].split(':')[1]
        except IndexError:
            logging.warning(f"Zotero ID {bibl.attrib['zotero']} does not comply to standard.")
            zot_id = bibl.attrib['zotero']
        zotero = [e[zot_id] for e in zot_data if zot_id in e]
        if norm != "":
            try:
                list_of_refs.append(
                    f'<li><label class="bibl-short" for="bibl-{t[0]}-{i}">{norm}</label><input type="checkbox" id="bibl-{t[0]}-{i}"/><div class="bibl-long">{zotero[0]}</div></li>')
            except IndexError:
                logging.warning("Could not find Zotero entries.")
    if len(list_of_refs) > 0:
        return f'<ul>{"".join(list_of_refs)}</ul>'


def get_history_of_changes(xml: etree._Element) -> dict:
    """Retrieves the revision history of the research data..

    Args:
        xml: The root of the TEI-XML tree
    """
    change_elems = xml.findall(f".//{NS}change")
    if change_elems:
        changes = []
        for change in change_elems:
            date = change.attrib.get('when')
            try:
                text = normalize(change.text)
            except TypeError:
                logging.warning("Change element does not have any text.")
                text = '###'
            changes.append({'key': date, 'value': text})
        return {'key': 'Änderungshistorie', 'metadata': changes}
    logging.warning("No changes found.")
    return None


def string_from_mixed_content(el: etree._Element) -> str:
    """
    Creates a string containing links for XML mixed content elements.
    """
    result = []
    for child in el.xpath('node()'):
        if isinstance(child, etree._Element):
            if etree.QName(child).localname == 'ref':
                link = make_link_from_element(child, 'target')
                result.append(link)
            if etree.QName(child).localname == 'hi':
                if len(child.attrib) == 1:
                    if child.attrib['rend'] == 'super':
                        display_text = normalize(child.text)
                        result.append(f'<sup>{display_text}</sup>')
                else:
                    display_text = normalize(child.text)
                    result.append(f'<i>{display_text}</i>')
        else:
            result.append(child)
    joined = " ".join(result)
    normalized = normalize(joined)

    if '<sup>' in normalized:
        return normalized.replace(' <sup>', '<sup>')
    return normalized


def make_link_from_element(el: etree._Element, href: str) -> str:
    """
    Creates an HTML string representing a link from an element that
    links to an (external) source.
    """
    try:
        display_text = normalize(el.text)
        target = el.attrib[href]
        return f'<a href="{target}" target="_blank">{display_text}</a>'
    except TypeError:
        logging.warning(f'Link in {el} does not have any text.')
        return '###'


def get_surrogates(xml: etree._Element) -> dict:
    """
    Returns all available graphical surrogates for the tablet as HTML list.

    Args:
        - xml: The root of the TEI-XML tree
    """
    surrogates = xml.findall(f'.//{NS}surrogates/{NS}graphic')
    if not surrogates:
        logging.warning("No surrogates found.")
    graphics = []

    for el in surrogates:
        graphics.append(f'<li>{make_link_from_element(el, "url")}</li>')

    return {'key': 'Fotos (WSRP / InscriptiFact)', 'value': f'<ul>{"".join(graphics)}</ul>'}


def get_citation_hint(norm_id: str, eupt_url: str, xml: etree._Element) -> dict:
    """
    Returns the citation hint for a tablet.

    Args:
        - norm_id: the normalized ID of the tablet, e.g. "KTU_1.14"
        - eupt_url: the URL of the current server, e.g. "http://eupt-dev.sub.uni-goettingen.de"
        - xml: The root of the TEI-XML tree
    """
    try:
        latest_version = xml.findall(f'.//{NS}revisionDesc/{NS}change')[0]
        date = latest_version.attrib.get('when')
        all_editors = xml.findall(f'.//{NS}editor')
        editors = make_editor_string(all_editors, latest_version, 'editor')
        coeditors = make_editor_string(all_editors, latest_version, 'coEditor')
        link = f'<a href="{eupt_url}" target="_blank">{eupt_url}</a>'
        title = '<i>Edition des ugaritischen poetischen Textkorpus (EUPT)</i>'
        editors = f'Bearbeitet von {editors}, unter Mitarbeit von {coeditors}.<div class="padding"></div>'
        display_id = norm_id.replace('_', ' ')
        cit = f'{display_id}. {title}. {latest_version.text}. {link} [{date}]. {editors}'
    except AttributeError:
        cit = "TODO"

    return {'key': 'Zitationshinweis', 'value': cit}


def make_editor_string(all_editors: list, latest_version: etree.Element, e_type: str) -> str:
    """
    Creates the string indicating who has worked on the current version of the edition.
    
    Args:
        - all_editors: all tei:editor elements in the TEI header
        - latest_version: the first (= latest) tei:chance element in the tei:revisionDesc
        - e_type: the type of the editor. can be 'editor' or 'coEditor'
        
    Return:
        - the name of the editor if only one is found
        - the names of the editors involved in the form 'A, B, C und D'
    """
    refs = [name for name in latest_version.attrib.get(e_type).replace('#', '').split(' ')]
    full = []
    for r in refs:
        for entry in all_editors:
            if entry.attrib.get(f'{XML_NS}id') == r:
                forename = entry.findall(f'.//{NS}forename')[0].text
                surname = entry.findall(f'.//{NS}surname')[0].text
                name = f'{forename} {surname}'
                full.append(name)
    if len(full) != 1:
        first = ', '.join(full[:-1])
        return f'{first} und {full[-1]}'
    return full[0]