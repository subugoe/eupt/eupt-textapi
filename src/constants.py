__all__ = [
    'JSONLD_URL',
    'API_PREFIX',
    'TEXTAPI_VERSION'
]

JSONLD_URL = 'https://gitlab.gwdg.de/subugoe/textapi/specs/-/raw/main/jsonld'
API_PREFIX = 'api/eupt'
TEXTAPI_VERSION = '1.1.0'
NS = '{http://sub.uni-goettingen.de/edxml#}'
TEI_NS = '{http://www.tei-c.org/ns/1.0}'
XML_NS = '{http://www.w3.org/XML/1998/namespace}'
