import os
from sys import argv
from re import compile, search

import requests
from tgclients import TextgridConfig, TextgridCrudRequest, TextgridSearch
from tgclients.config import PROD_SERVER
from lxml import etree

# initialise tgclients interfaces
TG_CONFIG = TextgridConfig(PROD_SERVER)
TG_SEARCH = TextgridSearch(TG_CONFIG, nonpublic=True)
TG_CRUD = TextgridCrudRequest(TG_CONFIG)
TG_USERNAME = os.environ['TG_USERNAME']
TG_PASSWORD = os.environ['TG_PASSWORD']
TG_PROJECT_ID = os.environ['TG_PROJECT_ID']

def store_xmls(main_only: bool=True) -> None:
    """
    Stores all XMLs for the EUPT in assets/xml.

    Args:
        main_only: True if only the main XML should be fetched

    Returns:
        XML of the corresponding main file, e.g. "a_KTU 1.14.main",
        or `None` if no (unique) corresponding main file was found.

    """
    try:
        # create a textgrid sid
        # the SID service returns the SID encapsuled in double quotes.
        # this leads to an error when authenticating.
        TG_SID = requests.post(
            "https://sid.eupt-dev.sub.uni-goettingen.de/",
            json={"username": TG_USERNAME, "password": TG_PASSWORD},
            timeout=20
        ).text.strip('"')

        results = TG_SEARCH.search(
            filters=["project.id:" + TG_PROJECT_ID],
            limit=500,
            sid=TG_SID
        ).result
        aggregations = [res.object_value.generic.generated.textgrid_uri.value for res in results]

        main_xml_uris = get_xml_uris(aggregations, TG_SID, True)
        pub_status = {}
        for uri in main_xml_uris:
            xml = TG_CRUD.read_data(uri, sid=TG_SID).text
            title = get_title(uri, TG_SID)
            short_title = get_short_title(title)
            pub_stat = search(compile('docStatus status="(.+)"'), xml).group(1)
            pub_status[short_title] = pub_stat
            store_according_to_pub_status(xml, uri, title, pub_status)

        xml_uris = get_xml_uris(aggregations, TG_SID, main_only)
        xml_uris_wo_mains = set(xml_uris) - set(main_xml_uris)

        for uri in xml_uris_wo_mains:
            xml = TG_CRUD.read_data(uri, sid=TG_SID).text
            title = get_title(uri, TG_SID)
            store_according_to_pub_status(xml, uri, title, pub_status)

    except AssertionError:
        pass

def store_according_to_pub_status(xml: str, uri: str, title: str, pub_status: dict) -> None:
    short_title = get_short_title(title)
    filename = f'{title}.{uri[9:]}'
    if pub_status[short_title] == 'published':
        directory = 'assets/xml'
    else:
        directory = 'private-assets/xml'
    with open(f'{directory}/{filename}.xml', mode='w', encoding='utf-8') as f:
        f.write(xml)

    print(f'Successfully saved XMLs for {title} to {directory}.')

def get_title(uri: str, TG_SID: str) -> str:
    """
    Retrieves a file's title according to its TG metadata.
    """
    metadata = TG_CRUD.read_metadata(uri, sid=TG_SID).text.encode('utf-8')
    parser = etree.XMLParser(encoding='utf-8')
    meta_xml = etree.fromstring(metadata, parser=parser)
    NS = '{http://textgrid.info/namespaces/metadata/core/2010}'
    raw_title = meta_xml.find(f".//{NS}title").text
    return raw_title.replace(' ', '_')

def get_short_title(title: str) -> str:
    """
    Generates a short title, e.g.

    - 'a_KTU 1.14.main'     --> 'KTU 1.14'
    - 'b_KTU 1.4 I.include' --> 'KTU 1.4'
    - 'a_RS 12.2345.main'   --> 'RS 12.2345'
    """
    if "KTU" in title:
        pattern = compile('KTU_1.[0-9]+')
    else:
        pattern = compile('RS_[0-9]+.[0-9]+')
    res = search(pattern, title)
    return res.group(0).replace('_', ' ')


def get_xml_uris(aggregations: list, TG_SID: str, main_only: bool) -> list:
    """
    Returns the URIs of XMLs belonging to the EUPT project.

    Args:
        - main_only: True if only the main file should be fetched.
    """
    xml_uris = []
    for aggregation_uri in aggregations:
        for child in TG_SEARCH.children(aggregation_uri, sid=TG_SID).textgrid_uri:
            child_results = TG_SEARCH.info(textgrid_uri=child, sid=TG_SID).result
            if len(child_results) == 1:
                cg = child_results[0].object_value.generic
                try:
                    pattern = compile('KTU|RS')
                    res = search(pattern, cg.provided.title[0])
                    if (cg.provided.format == "text/xml") and (
                        (not main_only) or cg.provided.title[0].endswith("main")
                        ) and res.group(0):
                        child_uri = cg.generated.textgrid_uri.value
                        xml_uris.append(child_uri)
                except AttributeError:
                    pass
            else:
                pass
    return xml_uris

if __name__ == '__main__':
    try:
        main_only_flag = argv[1]
        if any(x in main_only_flag for x in ['false', 'False']):
            MAIN_ONLY_FLAG = False
        else:
            MAIN_ONLY_FLAG = True
        store_xmls(MAIN_ONLY_FLAG)
    except IndexError:
        store_xmls()
