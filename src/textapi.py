import logging
import json
import xml.etree.ElementTree as ET
from os.path import exists, isfile
from pathlib import Path
from fastapi import FastAPI, Request, Response
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import FileResponse, HTMLResponse
from collection import make_collection
from item import make_item
from manifest import make_manifest


app = FastAPI()
origins = [
    'https://eupt-dev.sub.uni-goettingen.de',
    'https://eupt-test.sub.uni-goettingen.de',
    'https://eupt.uni-goettingen.de'
]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_methods=['*'],
    allow_headers=['*'],
)

logging.basicConfig(format='{asctime} \t {levelname}: {message}',
                    style='{')

with open('assets/json/eupt-bibliography.json', mode='r', encoding='utf-8') as f:
    ZOTERO_DATA = json.loads(f.read())

@app.get('/healthz')
def get_health():
    """
    Endpoint for K8s liveness probe.
    """
    return {'status': 'ok'}


@app.get('/api/eupt/collection.json')
async def get_collection(request: Request):
    """
    Returns the TextAPI Collection Object for the EUPT project.
    """

    eupt_url = fix_base_url_protocol(request.base_url)
    eupt_server = request.url.hostname.split(".")[0]
    return make_collection(eupt_url, eupt_server)


@app.get('/api/eupt/html/{file_name}.html', response_class=HTMLResponse)
async def get_html(file_name: str, request: Request):
    """
    Returns an HTML file.

    Args:
        - file_name: The name of the file w/o extension
    """
    html_path = Path(f'assets/html/{file_name}.html')
    if exists(html_path):
        with open(html_path, mode='r', encoding='utf-8') as html_file:
            result = html_file.read()
    else:
        result = 'Requested file not available.'
    return fix_css_path(result, request.base_url)


@app.get('/api/eupt/css/{file_name}.css')
async def get_css(file_name: str):
    """
    Returns the CSS needed for styling the HTML files

    Args:
        - file_name: The name of the file w/o extension
    """    
    css_path = Path(f'assets/css/{file_name}.css')
    if exists(css_path):
        return FileResponse(css_path)
    logging.warning(f'Requested file {file_name}.css not available.')


@app.get('/api/eupt/img/{file_name}')
async def get_img(file_name: str):
    """
    Returns an image file.

    Args:
        - file_name: The name of the file w/o extension
    """
    img_path = Path(f'assets/img/{file_name}.svg')   
    if isfile(img_path):
        return FileResponse(img_path)
    logging.warning(f'Requested image {file_name} not available.')

@app.get('/api/eupt/{manifest}/manifest.json')
async def get_manifest(manifest: str, request: Request):
    """
    Returns a TextAPI Manifest Object for the EUPT project.

    Args:
        - manifest: the ID of the manifest, e.g. "KTU__1.14"
    """
    eupt_url = fix_base_url_protocol(request.base_url)
    return make_manifest(manifest, eupt_url)


@app.get('/api/eupt/{manifest}/1/full.json')
async def get_item(manifest: str, request: Request):
    """
    Returns a TextAPI Item Object for the EUPT project.

    Args:
        - manifest: the ID of the manifest, e.g. "KTU__1.14"
    """
    eupt_url = fix_base_url_protocol(request.base_url)
    return make_item(manifest, eupt_url)


@app.get('/api/eupt/biblio/{zotero_id}')
def get_zotero_entry(zotero_id: str):
    """
    Returns the marked up bibliographic entry for a given Zotero item.

    Args:
        - zotero_id: the ID of the Zotero item
    """
    try:
        return [e[zotero_id] for e in ZOTERO_DATA if zotero_id in e][0]
    except IndexError:
        logging.warning(f"No entry found for Zotero ID {zotero_id}")


@app.get('/api/eupt/biblio/xml/{zotero_id}')
def get_zotero_entry_as_xml(zotero_id: str):
    """
    Returns the marked up bibliographic entry for a given Zotero item in XML format.
    This is needed for our XSLTs to work smoothly.

    Args:
        - zotero_id: the ID of the Zotero item
    """
    try:
        entry = [e[zotero_id] for e in ZOTERO_DATA if zotero_id in e][0]
        xml_data = f'<?xml version="1.0"?><bibl xmlns="http://sub.uni-goettingen.de/edxml#">{entry}</bibl>'
        return Response(content=xml_data, media_type="application/xml")
    except IndexError:
        logging.warning(f"No entry found for Zotero ID {zotero_id}")


def fix_base_url_protocol(base_url: str) -> str:
    """
    request.base_url returns the http protcol instead of https which results
    in a "mixed content" error. This could not be fixed by the CORS middleware,
    so we simply replace the protocol here.
    This replacement only takes place when deployed; local instances are not
    affected.
    """
    if 'localhost' not in str(base_url):
        return str(base_url).replace('http:', 'https:')
    return base_url


def fix_css_path(html_str: str, base_url: str) -> str:
    s_base_url = str(base_url).replace('http:', 'https:')
    return html_str.replace('../css', f'{s_base_url}api/eupt/css')
