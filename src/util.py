from os import walk
from re import compile as comp
from typing import Any
from lxml import etree
import logging

from constants import NS

ROOT_DIR = "/code/assets/xml"
logging.basicConfig(format="{levelname}: {pathname} {lineno} - {message}", style="{")

def get_xml_file(manifest_id: str) -> Any:
    """
    Returns the root element of the manifest's XML file.
    """
    norm_id = get_normalized_id(manifest_id)
    regex = comp(f'a_{norm_id}.main.*$')

    for root, dirs, files in walk(ROOT_DIR):
        for file in files:
            if regex.match(file):
                main_file = file
    try:
        return open_file_as_xml(main_file)
    except UnboundLocalError:
        pass


def open_file_as_xml(file_name: str) -> Any:
    """
    Opens a file in the ROOT_DIR as XML.

    Return: The root element of the XML.
    """
    with open(f'{ROOT_DIR}/{file_name}', mode='r', encoding='utf-8') as f:
        xml_s = f.read()
        return etree.fromstring(bytes(xml_s, encoding='utf-8'))


def get_published_main_file_names() -> list:
    """
    Returns a list of all main file names that have been published.
    """
    main_file_names = get_all_main_xml_file_names()
    published_main_file_names = []

    for name in main_file_names:
        if has_file_been_published(open_file_as_xml(name)):
            published_main_file_names.append(name)
    return published_main_file_names


def get_published_manifest_ids() -> list:
    """
    Returns a list of all manifest IDs that have been published.
    The manifest IDs have the form "KTU__1.14".
    The list is sorted alphabetically.
    """
    published = get_published_main_file_names()
    manifest_ids = []
    for name in published:
        # "a_KTU_1.14.main.41tvp.0.xml" --> "KTU__1.14"
        manifest_id = name.split('a_')[1].split('.main')[0].replace('_', '__')
        manifest_ids.append(manifest_id)
    return manifest_ids


def get_all_manifest_ids() -> list:
    """
    Returns a list of all manifest IDs, irregardless of publication status.
    The list is sorted alphabetically.
    """
    main_file_names = get_all_main_xml_file_names()
    manifest_ids = []
    for name in main_file_names:
        # "a_KTU_1.14.main.41tvp.0.xml" --> "KTU__1.14"
        manifest_id = name.split('a_')[1].split('.main')[0].split('_main')[0].replace('_', '__')
        try:
            # we open the XML file once to check if there are any errors lxml might complain about
            get_xml_file(manifest_id)
            manifest_ids.append(manifest_id)
        except etree.XMLSyntaxError:
            logging.warning(f'XML with id {manifest_id} could not be opened.')
            pass
    return manifest_ids

def get_published_main_xml_files() -> list:
    """
    Returns a list of root elements for the available published main XML files.
    """
    main_xmls = get_all_main_xml_files()
    published_mains = []
    for xml in main_xmls:
        if has_file_been_published(xml):
            published_mains.append(xml)
    return published_mains


def has_file_been_published(xml: Any) -> bool:
    """
    Whether a file has the publication status "published".
    """
    publication_status = xml.find(f".//{NS}docStatus").attrib['status']
    return publication_status == "published"


def get_all_main_xml_file_names() -> list:
    """
    Returns a list of file names of all available XML files.
    """
    main_file_names = []
    regex = comp('a_.*?.main.*$')
    for root, dirs, files in walk(ROOT_DIR):
        for file in files:
            if regex.match(file):
                main_file_names.append(file)
    return main_file_names


def get_all_main_xml_files() -> list:
    """
    Returns a list of root elements for all available main XML files.
    """
    main_file_names = get_all_main_xml_file_names()
    xmls = []
    for file_name in main_file_names:
        with open(f'{ROOT_DIR}/{file_name}', mode='r', encoding='utf-8') as f:
            xml_s = f.read()
            xmls.append(etree.fromstring(bytes(xml_s, encoding='utf-8')))
    return xmls


def get_normalized_id(manifest_id: str) -> str:
    """
    Removes underscores from manifest id.
    """
    return manifest_id.replace('__', '_').replace('+', '\+')

def get_display_id(manifest_id: str) -> str:
    """
    Removes the underscores from the manifest id.
    """
    return manifest_id.replace('__', ' ')