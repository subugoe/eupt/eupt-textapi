"""
Load data from the Zotero Library and stores it in assets/json.
"""
import sys
from json import dumps
from pyzotero import zotero
import zotero_rendering.src.biblio as z

sys.path.append('code/zotero_rendering')


if __name__ == '__main__':
    LIBRARY_ID = '5113405'
    LIBRARY_TYPE = 'group'
    zot = zotero.Zotero(LIBRARY_ID, LIBRARY_TYPE, '')
    data = zot.everything(zot.items())
    slim_data = []

    # create slim data
    for item in data:
        new_data = {}
        for key in item['data']:
            if item['data'][key] == '':
                pass
            else:
                new_data[key] = item['data'][key]
        new_entry = {'key': item['key'],
                     'data': new_data}
        slim_data.append(new_entry)

    # prerendering
    prerendered = []
    for item in slim_data:
        print(item['key'])
        prerend_item = {item['key']: str(
            z.make_ref_by_id(item['key'], slim_data))}
        prerendered.append(prerend_item)

    with open('assets/json/eupt-bibliography.json', mode='w', encoding='utf-8') as f:
        f.write(dumps(prerendered))
