"""
This module is responsible for creating a complete Manifest Object
for the EUPT project.
"""

from lxml import etree
from constants import API_PREFIX, JSONLD_URL, TEXTAPI_VERSION, TEI_NS
from manifest_metadata import get_metadata
from util import get_xml_file, get_display_id
from datetime import datetime


def make_manifest(manifest_id: str, eupt_url: str) -> dict:
    """
    Creates a Manifest Object for the EUPT project.
    """
    xml = get_xml_file(manifest_id)
    
    return {
        '@context': f'{JSONLD_URL}/manifest.jsonld',
        'textapi': TEXTAPI_VERSION,
        'id': f'{eupt_url}{API_PREFIX}/{manifest_id}/manifest.json',
        'label': get_display_id(manifest_id),
        'sequence': get_sequence(manifest_id, eupt_url),
        'support': get_support_object(eupt_url),
        'license': get_license(xml),
        'metadata': get_metadata(manifest_id, eupt_url, xml)
    }


def get_sequence(manifest_id: str, eupt_url: str) -> list:
    """
    Creates the TextAPI Sequence Objects.
    """
    sequence = [
        {
            '@context': f'{JSONLD_URL}/sequence.jsonld',
            'id': f'{eupt_url}{API_PREFIX}/{manifest_id}/1/full.json',
            'type': 'item',
            'label': 'Volltext'
        }
    ]
    return sequence


def get_license(xml: etree._Element) -> list:
    """
    Returns information about the manifest's license.
    """
    license_el = xml.find(f'.//{TEI_NS}licence')
    license_link = license_el.attrib.get('target')
    return [{
        "id": "CC-BY-SA-4.0",
        "notes": f'See <a href="{license_link}" target="_blank">License</a> for more information.'
    }]


def get_support_object(eupt_url: str) -> list:
    """
    Creates a TextAPI Support Object that currently includes a CSS file.
    Might be enhanced to hold fonts in the future.
    """
    css = []
    # we add the current date to force browsers to use the
    # latest CSS instead of one from the cache.
    # see https://gitlab.gwdg.de/subugoe/eupt/eupt-textapi/-/issues/32
    timestamp = datetime.today().strftime('%Y-%m-%d')
    for name in ['styles', 'metadata']:
        obj = {
            '@context': f'{JSONLD_URL}/support.jsonld',
            'type': 'css',
            'mime': 'text/css',
            'url': f'{eupt_url}{API_PREFIX}/css/{name}.css?date={timestamp}'
        }
        css.append(obj)
    return css
